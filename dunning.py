# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
"account_dunning_separate_each_level"
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


class Dunning(ModelSQL, ModelView):
    'Dunning'
    _name = 'account.dunning'

    def build_dunning(self, valuation_date=None, move_line=None,
        level_id=None, block=False, parent_id=None):
        '''
        Build method for adding new dunnings and dunning lines to dunnings.
        This method build for each party and level one and only one
        dunning with several lines of the same level_id.
        This method check if there is already a dunning for a given party
        (move_line.party) and level_id. If not, the dunning for the party
        is added.
        After this, the dunning line is added to the appropriate dunning.

        :param self.proposal_list: class attribute, list of dunning proposals.
        :param valuation_date: the effective date for creating the proposals
        :param move_line: a singular move line object overdue.
        :param level_id: the proposed level for the dunning line
        :param block: Bool which indicates blocked dunning lines
        :param parent_id: the id of a parent confirmed dunning line
        '''
        state = 'proposed'
        is_added = False
        # Append new dunning proposal
        for idx, dunning in enumerate(self.proposal_list):
            if move_line.party.id == dunning['party']:
                for line in dunning['lines']:
                    if is_added:
                        break

                    if line[1]['level'] == level_id:
                        self._add_dunning_line(idx, valuation_date, move_line,
                                level_id, block, parent_id, state)
                        is_added = True

            if is_added:
                break

        if not is_added:
            address = self.address_get(move_line.party)
            self._add_dunning(valuation_date, move_line, address, state)
            idx = self.proposal_list.index([i for i in self.proposal_list \
                    if move_line.party.id == i['party'] and not i['lines']][0])

            # Append new dunning proposal line
            self._add_dunning_line(idx, valuation_date, move_line, level_id,
                    block, parent_id, state)

    def _add_payment_lines(self, payment_move_lines=None, valuation_date=None):
        '''
        Add payment lines to class variable self.proposal_list.

        :param self.proposal_list: class attribute, list of dunning proposals.
        :param payment_move_lines: a list of move lines lines paid.
        :param valuation_date: the effective date for creating the proposals

        '''
        move_line_obj = Pool().get('account.move.line')
        invoice_payment_line_obj = Pool().get(
            'account.invoice-account.move.line')
        # Append all payment lines that can be related to an invoice
        payment_line_ids = [line['id'] for line in payment_move_lines]
        id2credit = {}
        for line in payment_move_lines:
            id2credit[line['id']] = line['credit']
        for idx, proposal in enumerate(self.proposal_list):
            move_line_ids = [line[1]['move_line'] for line in
                proposal['lines']]
            move_lines = move_line_obj.search_read([
                    ('id', 'in', move_line_ids),
                ], fields_names=['move.invoice'])
            invoices = set([])
            for line in move_lines:
                if line['move.invoice']:
                    invoices.add(line['move.invoice'][0])
            matching_payment_lines = invoice_payment_line_obj.search_read([
                ('invoice', 'in', [x for x in invoices]),
                ('line', 'in', payment_line_ids)
                ], fields_names=['line'])
            for line in matching_payment_lines:
                self.proposal_list[idx]['payments'].append(
                    tuple(['create', {
                        'move_line': line['line'],
                        'date': valuation_date,
                        'amount': id2credit[line['line']],
                    }]))

    def _add_undue_dunning_lines(self, undue_lines=None, valuation_date=None):
        '''
        Do not add undue dunning lines in this proposal.
        '''
        pass

Dunning()
