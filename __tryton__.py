#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Dunning Separate each Level',
    'name_de_DE': 'Buchhaltung Mahnwesen Getrennte Mahnstufen',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Extend trytond_account_dunning with a method to generate
      separated dunning proposals for each level.
''',
    'description_de_DE': '''
    - Erweitert das Mahnwesen-Modul um eine Methode, um Mahnungen
      jeweils getrennt nach Mahnstufe erzeugen zu können.
''',
    'depends': [
        'account_dunning',
        'account_move_invoice'
    ],
    'xml': [
        'dunning.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
